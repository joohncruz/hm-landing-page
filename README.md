# Landing Page

Projeto consiste em uma Landing Page(reduzida) para o produto Hotmart Club, com uma aplicação que mostra o número de novos clientes que a ferramenta obteve no último mês.

# Stack

Seguindo a premissa de ter uma Landing Page, desenvolvida em cima de componentes com o máximo possível de reutilização de código consistindo em uma velocidade de renderização, foi decidido utilizar a biblioteca VueJS, por ter uma curva de aprendizado pequena e consistir todas as premissas anteriores. 

Para estilização não foi utilizada nenhuma lib externa, criando assim uma estrutura simples, pequena e reaproveitável de CSS, onde o sistema de grid foi criado utilizando display grid-layout, e cada componente teve o seu respectivo estilo associado. 

## Estrutura 

A estrutura inicial criada para o projeto foi criada a partir do [vue-cli](https://github.com/vuejs/vue-cli/tree/master), com a estrutura de webpack.

**Configurações:** 
- Eslint - Standard ( por ter uma flexibilidade maior e uma simples configuração )
- Unit Tests e Test Runner - Jest ( simples configuração, feedback rapido e possibilidade de utilizar snapshots )
- NPM

**Componentes:** 

Foi realizado uma pequena customização na estrutura inicial do vue-cli, estrutura passou a consistir em: 

    > src
      > components
	    > section-header  
			.section-header.vue
			.section-header.spec.js	
		> shared
			> button 
				.button.vue
				.button.spec.js

Podemos observar neste exemplo que o "section-header" seria um componente pai onde não seria reutilizado por outras paginas, e nesta pasta podemos observar um arquivo com a extensão .vue e outra .spec.js, fazendo assim o teste do componente ficar na mesma pasta do mesmo ajudando na visualização e identificação de se um componente tem teste ou não. 

Exemplo de um teste em um componente: 

    import Vue from 'vue'
	import HelloWorld from '@/components/HelloWorld'

	describe('HelloWorld.vue', () => {
	  it('should render correct contents', () => {
	    const Constructor = Vue.extend(HelloWorld)
	    const vm = new Constructor().$mount()
	    expect(vm.$el.querySelector('.hello h1').textContent)
	      .toEqual('Welcome to Your Vue.js App')
	  })
	})


## Build Setup

Para utilização do projeto verifique se você tem instalado o Node.js e o npm no seu computador pelos comandos “node -v” e “npm -v”. Para instalar essa ferramentas consulte a documentação disponível em (https://nodejs.org/en/) e (https://www.npmjs.com/).

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```
Para mais detalhes consulte o  [guide](http://vuejs-templates.github.io/webpack/) e [docs for vue-loader](http://vuejs.github.io/vue-loader).

O Bash irá informar o localhost criado para a aplicação.